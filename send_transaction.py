from transaction import TXInput, TXOutput, Transaction
from errors import NotEnoughAmountError
from utils import address_to_pubkey_hash
from wallets import Wallets
from utxopool_operation import get_utxos_amount, get_utxos


def new_transaction(from_addr, to_addr, amount):
    inputs = []
    outputs = []

    wallets = Wallets()
    from_wallet = wallets[from_addr]
    pub_key = from_wallet.public_key

    acc = get_utxos_amount(from_addr)
    valid_outpus = get_utxos(from_addr)
    if acc < amount:
        raise NotEnoughAmountError(u'not enough coin')

    for output in valid_outpus:
        outindex = output['outindex']
        txid = output['txid']
        tx_input = TXInput(txid, outindex, pub_key)
        inputs.append(tx_input)

    output = TXOutput(amount, to_addr)
    outputs.append(output)
    if acc > amount:
        # a change
        outputs.append(TXOutput(acc - amount, from_addr))

    tx = Transaction(inputs, outputs)
    tx.set_id()
    sign_transaction(tx, from_wallet.private_key)
    return tx


def coin_base_tx(to_addr):
    tx = Transaction.coinbase_tx(to_addr, '')
    return tx


# def find_transaction(self, txid):
#     return None


def sign_transaction(tx, priv_key):
    tx.sign(priv_key)


def verify_transaction(tx):
    return tx.verify()

import hashlib
import random
import time as t

import pymysql

import Global
import blockchain_pb2
from transaction import Transaction, TXOutput, TXInput
import json
# This file used to define the blockchain structure

# A block must include hash code (address), previous block's hash code, and the transaction context
class Block:
    def __init__(self):
        self.Hash = ''
        self.Transaction = {}
        self.PrevBlockHash = ''
        self.Index = ''
        self.Nonce = ''
        self.Difficulty = ''
        self.TimeStamp = ''


# blockchain class is a list to append the block one by one.
class Blockchain:
    def __init__(self):
        self.blocks = []
        self.difficulty = 10
        # when a new blockchain class was implement, the genesis block will be created as first block
        genesis_block = NewBlock(json.dumps('Genesis Block'), '', '11:58:03,2022-11-16', 0, 0, 0)
        self.blocks.append(genesis_block)
        self.target_hash = 0

# nodes in the blockchain.
class NodePool:
    def __int__(self):
        self.nodes = []
        # when started, add itself to the pool
        selfNode = input('Please enter your node address: ')
        print("test")
        self.nodes.append(selfNode)


# function of create a new block
def NewBlock(transaction: str, prevBlockHash: str, timeStamp: str, nonce: int, index: int,
             difficulty: int):
    # new block hash (address) generation should include the previous block's hash

    decode_tx = json.loads(transaction)
    print(decode_tx)
    hash_code = str(transaction) + str(prevBlockHash) + str(timeStamp) + str(nonce)
    hash = hashlib.sha256(hash_code.encode("utf-8")).hexdigest()
    # declare a block with Block class
    block = Block()
    block.Hash = hash
    block.Transaction = transaction
    block.PrevBlockHash = prevBlockHash
    block.Index = index
    block.Nonce = nonce
    block.Difficulty = difficulty
    block.TimeStamp = timeStamp
    return block

# function of append a new block with the blockchain
def AddBlock(blockchain: Blockchain, transaction: str, expect_hash: int, time: str):
    # if the miner give the correct puzzle answer (target_hash), then the miner's transaction will be accepted and added at new block
    # if the miner give the correct puzzle answer (target_hash), then the miner's transaction will be accepted and added at new block
    dbhost = 'localhost'
    dbuser = 'root'
    dbpass = ''
    dbname = 'test'
   
    if expect_hash == blockchain.target_hash:
        prevBlockHash = blockchain.blocks[-1].Hash
        index = len(blockchain.blocks)
        difficulty = blockchain.difficulty
        block = NewBlock(transaction=transaction, prevBlockHash=prevBlockHash, timeStamp=time,
                         difficulty=difficulty, index=index, nonce=expect_hash)
        blockchain.blocks.append(block)
        # Describe the status at terminal
        print("Blockchain updated: ")
        for i in range(len(blockchain.blocks)):
            print("block " + str(i) + ":")
            print('block hash = ' + blockchain.blocks[i].Hash)
            print('block transaction = ' + blockchain.blocks[i].Transaction)
            print('block time stamp = ' + blockchain.blocks[i].TimeStamp)
            print('block difficulty = ' + str(blockchain.blocks[i].Difficulty))
            print('block previous hash = ' + str(blockchain.blocks[i].PrevBlockHash))
            sqlQuery = "INSERT IGNORE INTO BLOCKCHAIN (Height, TimeStamp, Diffculty , Nonce, Transaction, Hash, PrevBlockHash) VALUE(%s,%s,%s,%s,%s,%s,%s)"
            value = (str(blockchain.blocks[i].Index), str(blockchain.blocks[i].TimeStamp),str(blockchain.blocks[i].Difficulty),
                    str(blockchain.blocks[i].Nonce),str(blockchain.blocks[i].Transaction), str(blockchain.blocks[i].Hash),
                    str(blockchain.blocks[i].PrevBlockHash))
            try:
                db = pymysql.connect(host=dbhost, user=dbuser, password=dbpass, database=dbname)
                cur = db.cursor()
                cur.execute(sqlQuery,value)
                db.commit()
                print("insert successfully")
            except pymysql.Error as e:  
                print("insert failed:"+ str(e))
                db.rollback()
        print()
        #set the new target hash. 
        TIME_SPAN = 100
        current_time = t.time()
        blockchain.target_hash = random.randint(0, 9)
        if int((current_time - Global.start_time)/TIME_SPAN) > Global.prev_index:
            Global.prev_index = int((current_time - Global.start_time) / TIME_SPAN)
            Global.newrandom = (Global.thre - Global.prev_index)
            if Global.newrandom <= 0:
                Global.prev_index = 0
                Global.newrandom = (Global.thre - Global.prev_index)

        print(Global.newrandom)
        blockchain.target_hash = random.randint(0,Global.newrandom)
        print("Next Block's Puzzle Hash is " + str(blockchain.target_hash))
        return blockchain.blocks[-1].Hash
    # if the miner give the wrong puzzle answer, return false.
    else:
        return "False"

def SynBlockchain(block: str):
    decode_tx = json.loads(block)
    new_block = NewBlock()
# Encapsulate the function of creating a blockchain into NewBlockchain
def NewBlockchain():
    new_blockchain = Blockchain()
    return new_blockchain

# Create the new node pool 
def NewNodePool():
    new_nodePool = []
    return new_nodePool

import pymysql
# 用来创建存储unspent outputs 的池子 utxopool

# create utxopool datatable
conn = pymysql.connect(
    host="127.0.0.1",
    port=3306,
    database="test",
    charset="utf8",
    user="root",
    passwd=""
)

cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS utxopool")

sql = """CREATE TABLE utxopool(
id int(8) NOT NULL AUTO_INCREMENT,
address varchar(50) NOT NULL,
amount int(20) NOT NULL,
txid int(50) NOT NULL,
outindex int(20) NOT NULL,
PRIMARY KEY (id)   
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
"""

cursor.execute(sql)

conn.close()

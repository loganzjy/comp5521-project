import pymysql


# 针对utxo缓存池的操作


# 获取某个地址的所有utxo信息，输出一个字典列表 [{'txid': , 'outindex': , 'amount': },]
def get_utxos(add):
    # call database
    conn = pymysql.connect(
        host="127.0.0.1",
        port=3306,
        database="test",
        charset="utf8",
        user="root",
        passwd=""
    )
    cursor = conn.cursor(pymysql.cursors.DictCursor)

    # get utxo
    sql = "select txid,outindex,amount from utxopool where address = %s"
    cursor.execute(sql, add)
    res = cursor.fetchall()
    return res


# 获取某个地址的所有utxo值的和，输出一个int
def get_utxos_amount(add):
    # call database
    conn = pymysql.connect(
        host="127.0.0.1",
        port=3306,
        database="test",
        charset="utf8",
        user="root",
        passwd=""
    )
    cursor = conn.cursor()

    # get utxo
    sql = "select amount from utxopool where address = %s"
    cursor.execute(sql, add)
    res = cursor.fetchall()
    amount = sum(map(sum, res))
    return amount


# 给某个地址添加utxo（挖矿的bonus使用、生成coinbase）
def coinbase_tx(add, txid):
    # call database
    conn = pymysql.connect(
        host="127.0.0.1",
        port=3306,
        database="test",
        charset="utf8",
        user="root",
        passwd=""
    )
    cursor = conn.cursor()

    # add utxo for coinbase_tx
    data = (add, "200", txid, "0")
    sql = 'insert into utxopool (address,amount,txid,outindex) VALUES (%s,%s,%s,%s)'
    cursor.execute(sql, data)
    conn.commit()
    conn.close()


# 验证tx成功时使用，用来更新utxo池,
def make_transaction(fromAdd, toAdd, amount, txid):
    # call database
    conn = pymysql.connect(
        host="127.0.0.1",
        port=3306,
        database="test",
        charset="utf8",
        user="root",
        passwd=""
    )
    cursor = conn.cursor()
    # 获取账户余额
    acc = get_utxos_amount(fromAdd)

    # 分两种情况，如果余额够，则不找零；余额够则需要找零
    if acc == amount:
        sql1 = 'delete from utxopool where address = %s'
        cursor.execute(sql1, fromAdd)
        conn.commit()

        sql2 = 'insert into utxopool (address,amount,txid,outindex) VALUES (%s,%s,%s,%s)'
        data2 = (toAdd, str(amount), str(txid), "0")  # 余额正好没有找零
        cursor.execute(sql2, data2)
        conn.commit()
    else:
        sql3 = 'delete from utxopool where address = %s'
        cursor.execute(sql3, fromAdd)
        conn.commit()

        sql4 = 'insert into utxopool (address,amount,txid,outindex) VALUES (%s,%s,%s,%s)'
        data4 = (toAdd, str(amount), str(txid), "0")  # 转账
        cursor.execute(sql4, data4)
        conn.commit()
        sql5 = 'insert into utxopool (address,amount,txid,outindex) VALUES (%s,%s,%s,%s)'
        data5 = (fromAdd, str(acc - amount), str(txid), "1")  # 找零
        cursor.execute(sql5, data5)
        conn.commit()


# 测试用
if __name__ == "__main__":
    add = '1N3XiHk82xmV5HnPRTfC1HBCHWY6yjFuq5'
    coinbase_tx(add,123)

    # make_transaction(add1, add2, 50, "0002")



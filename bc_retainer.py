from concurrent import futures
import logging
import datetime
import grpc
import blockchain_pb2
import blockchain_pb2_grpc
import time
from datetime import datetime as dt
from bc_definition import *
import json
from google.protobuf.json_format import MessageToJson


# We implement the function at server class
class BlockchainServer(blockchain_pb2_grpc.BlockChainServicer):
    # When the server created, a new blockchain will be created too
    def __init__(self):
        self.blockchain = NewBlockchain()
        self.nodes = NewNodePool()
        self.node =''

    # Receive miner's transaction and expected hash. And return the result or the address of the new block to the miner.
    def AddBlock(self, request, context):
        # hash_code = AddBlock()

        tx = json.loads(request.transaction)
        tx_de = Transaction.deserialize(tx)
        status = tx_de.verify()
        if(status):
            hash_code = AddBlock(self.blockchain, request.transaction, request.expectHash, request.time)
        return blockchain_pb2.AddBlockResponse(hash=hash_code)

    # Receive the miner's query request and return the complete blockchain (list format)
    def QueryBlockchain(self, request, context):
        response = blockchain_pb2.QueryBlockchainResponse()
        for block in self.blockchain.blocks:
            pb2_block = blockchain_pb2.Block(transaction=block.Transaction, hash=block.Hash,
                                             prevBlockHash=block.PrevBlockHash, index=int(block.Index),
                                             nonce=int(block.Nonce), difficulty=int(block.Difficulty),
                                             time=block.TimeStamp)
            response.blocks.append(pb2_block)
        return response

    def QueryBlock(self, request, context):
        response = blockchain_pb2.QueryBlockResponse()
        # TODO
        block = self.blockchain.blocks[-1]
        Block = blockchain_pb2.Block(transaction=block.Transaction, hash=block.Hash, prevBlockHash=block.PrevBlockHash)
        return blockchain_pb2.QueryBlockResponse(block=Block)

    def BuildNodeNetwork(self, request, context):

        if(request.node == ''):
            nodes = {"nodes": self.nodes}
            nodes_json = json.dumps(nodes)
            print(type(nodes_json))
            response = blockchain_pb2.BuildResponse(nodes=nodes_json)
            return response
        handle = json.loads(request.node)
        print(handle)
        if len(self.nodes) ==0:
            self.nodes.append(handle.get('target',''))
        for node in self.nodes:
            if (node != handle.get('node_from','')):
                self.nodes.append(handle.get('node_from', ''))
                break
        nodes = {"nodes": self.nodes}
        nodes_json = json.dumps(nodes)
        print(type(nodes_json))
        response = blockchain_pb2.BuildResponse(nodes=nodes_json)

        return response

    def SendTransaction(self, request, context):
        response = blockchain_pb2.SendResponse

    def SynBlockchain(self, request, context):
        blockchian = MessageToJson(request)
        SynBlockchain(blockchian)


# server setting
def serve():
    # port = '50051'
    # address = '127.0.0.1:50051'
    address = input("Please enter your server address: ")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=30))
    blockchain_pb2_grpc.add_BlockChainServicer_to_server(BlockchainServer(), server)
    server.add_insecure_port(address)
    server.start()
    # BlockchainServer.BuildNodeNetwork(address)
    print("blockchain-demo started, listening on " + address)
    try:
        connect(address)
    except EnvironmentError:
        print('why!!!!')
    server.wait_for_termination()

def connect(node:str):
    target = input("Please enter the address you want to connect to: ")
    request = {"target": target, "node_from": node}
    request = json.dumps(request)
    print(request)
    if (target != 'no'):
        with grpc.insecure_channel(target) as channel:
            stub = blockchain_pb2_grpc.BlockChainStub(channel)
            response = stub.BuildNodeNetwork(blockchain_pb2.BuildRequest(node=request))
            print(response.nodes)
            # serialized = MessageToJson(response.nodes)
            nodes_json = json.loads(response.nodes)
            nodes = nodes_json.get('nodes', '')
        if len(nodes) >2:
            try:
                with grpc.insecure_channel(nodes[2]) as channel:
                    stub_2 = blockchain_pb2_grpc.BlockChainStub(channel)
                    print(stub_2)
            except Exception:
                print('why!!!!')

            # nodes = stub.BuildNodeNetwork(blockchain_pb2.BuildRequest(node='127.0.0.1:50052'))
# run the server
if __name__ == '__main__':

    logging.basicConfig()
    serve()

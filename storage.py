import pymysql

dbhost = 'localhost'
dbuser = 'root'
dbpass = ''
dbname = 'test'

try:
    db=pymysql.connect(host=dbhost,user=dbuser,password=dbpass,database=dbname)
    print('connect successfully!')
    cur=db.cursor()
    cur.execute('DROP TABLE IF EXISTS BLOCKCHAIN')
    sqlQuery = 'CREATE TABLE BLOCKCHAIN(Transaction CHAR(100) NOT NULL, Hash CHAR(100) UNIQUE, PrevBlockHash CHAR(100), Index )'
    cur.execute(sqlQuery)
    print("create table successfully!")
except pymysql.Error as e:
    print("create table unsucessfully:"+str(e))

from wallet import Wallet
from send_transaction import new_transaction, coin_base_tx, sign_transaction, verify_transaction
from utils import address_to_pubkey_hash
from transaction import TXInput, TXOutput, Transaction
from wallets import Wallets


def main():
    w1 = Wallet.generate_wallet()
    ws = Wallets()
    ws[w1.address] = w1
    w2 = Wallet.generate_wallet()
    ws[w2.address] = w2
    ws.save()
    keys = list(ws.wallets.keys())
    w1 = ws[keys[0]]
    w2 = ws[keys[1]]
    print(w1, w2)
    print(w1.address, w2.address)


if __name__ == "__main__":
    # main()
    ws = Wallets()
    keys = list(ws.wallets.keys())
    w1 = ws[keys[0]]
    w2 = ws[keys[1]]
    print(w1, w2)
    print(w1.address, w2.address)
    tx = new_transaction(w1.address, w2.address, 17)
    print(tx.serialize())
    print(tx.verify())

from __future__ import print_function

import logging
import random
import time
import grpc
import blockchain_pb2
import blockchain_pb2_grpc
from datetime import datetime as dt
from concurrent import futures
import hashlib
from wallet import Wallet
from send_transaction import new_transaction, coin_base_tx, sign_transaction, verify_transaction
from utils import address_to_pubkey_hash
from transaction import TXInput, TXOutput, Transaction
from wallets import Wallets
import json

from bc_retainer import BlockchainServer


# The client request the AddBlock to server
def run():
    clientNum = input("Please enter your num: ")
    address = '127.0.0.1:'
    port = input("Please enter the port you want to connect to: ")
    #
    # response = node_pool
    #
    # for node in node_pool:
    #     with
    with grpc.insecure_channel(address + port) as channel:
        stub = blockchain_pb2_grpc.BlockChainStub(channel)
        print(port)
        tran_msg = "This block was added by client "+clientNum+"."
        expect_hash = random.randint(0,9)
        expect_nonce = random.randint(0,9)
        timeStamp = time.time()
        timeStamp = dt.fromtimestamp(timeStamp)
        timeStamp = timeStamp.strftime("%H:%M:%S,%Y-%m-%d")

        ws = Wallets()
        keys = list(ws.wallets.keys())
        w1 = ws[keys[0]]
        w2 = ws[keys[1]]
        print(w1, w2)
        print(w1.address, w2.address)
        tx = new_transaction(w1.address, w2.address, 17)
        print(tx.serialize())
        tx_tojson = json.dumps(tx.serialize())
        print(tx.verify())
        response = stub.AddBlock(blockchain_pb2.AddBlockRequest(transaction=tx_tojson,
                                                                expectHash=expect_hash,
                                                                time=timeStamp))
        while 1 == 1:
            time.sleep(1)
            expect_hash = random.randint(0,9)
            print("Mining the next Block, Guessing the expected puzzle hash is " + str(expect_hash))
            response = stub.AddBlock(blockchain_pb2.AddBlockRequest(transaction=tx_tojson,
                                                                    expectHash=expect_hash,
                                                                    time=timeStamp))
            if response.hash != 'False':
                print("Add the block successfully. The target puzzle Hash is " + str(expect_hash))
                print(" Received block address: " + response.hash)

def serve():
    port = '50051'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    blockchain_pb2_grpc.add_BlockChainServicer_to_server(BlockchainServer(), server)
    server.add_insecure_port('127.0.0.1:' + port)
    server.start()

if __name__ == '__main__':
    logging.basicConfig()
    run()

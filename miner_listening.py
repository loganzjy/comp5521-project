from __future__ import print_function

import logging

import grpc
import blockchain_pb2
import blockchain_pb2_grpc

from google.protobuf.json_format import MessageToJson
import json

# The client request the QueryBlockchain to server, transferring the message by Block data structure
def run():
    blockchain_node1 = {}
    blockchain_node2 = {}
    address = input('Please enter the address you want to monitor')
    with grpc.insecure_channel(address) as channel:
        stub = blockchain_pb2_grpc.BlockChainStub(channel)
        blockchain_res = stub.QueryBlockchain(blockchain_pb2.QueryBlockchainRequest(message=''))
        blockchain = MessageToJson(blockchain_res)
        nodelist_res = stub.BuildNodeNetwork(blockchain_pb2.BuildRequest(node=''))
        # nodelist_json = MessageToJson(nodelist_res)
        nodelist_dict = json.loads(nodelist_res.nodes)
        nodelist = nodelist_dict.get('nodes','')
        blockchain_node1 = json.loads(blockchain)
        blockchain_node1 = blockchain_node1.get("blocks","")
        print("Received Blockchain: ")
        for node in blockchain_node1:
            print(node)
    #
    with grpc.insecure_channel(nodelist[1]) as channel:
        stub = blockchain_pb2_grpc.BlockChainStub(channel)
        blockchain_res = stub.QueryBlockchain(blockchain_pb2.QueryBlockchainRequest(message=''))
        blockchain = MessageToJson(blockchain_res)
        blockchain_node2 = json.loads(blockchain)
        blockchain_node2 = blockchain_node2.get("blocks","")

    if (len(blockchain_node2) != len(blockchain_node1)):
        if(len(blockchain_node2)) > len(blockchain_node1):
            with grpc.insecure_channel(nodelist[1]) as channel:
                stub = blockchain_pb2_grpc.BlockChainStub(channel)
                diff = (len(blockchain_node2)) - len(blockchain_node1)
                for i in range((len(blockchain_node1)),diff):

                    stub.AddBlock(blockchain_node2[i])
        if(len(blockchain_node2)) < len(blockchain_node1):
            with grpc.insecure_channel(nodelist[0]) as channel:
                stub = blockchain_pb2_grpc.BlockChainStub(channel)
                diff = (len(blockchain_node1)) - len(blockchain_node2)
                for i in range((len(blockchain_node2)),diff):
                    print(blockchain_node1[i])



if __name__ == '__main__':
    logging.basicConfig()
    run()

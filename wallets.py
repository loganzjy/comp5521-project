import pickle
from wallet import Wallet


class Wallets(object):
    WALLET_FILE = 'wallet.dat'

    def __init__(self, wallet_file=WALLET_FILE):
        self.wallet_file = wallet_file
        try:
            with open(wallet_file, 'rb') as f:
                self.wallets = pickle.load(f)
        except IOError:
            self.wallets = {}

    def __getitem__(self, key):
        return self.wallets[key]

    def __setitem__(self, key, value):
        self.wallets[key] = value

    def get(self, key, default=None):
        return self.wallets.get(key, default)

    def save(self):
        with open(self.wallet_file, 'wb') as f:
            pickle.dump(self.wallets, f)

    def items(self):
        return self.wallets.items()


if __name__ == "__main__":
    w1 = Wallet.generate_wallet()
    ws = Wallets()
    ws[w1.address] = w1
    w2 = Wallet.generate_wallet()
    ws[w2.address] = w2
    ws.save()
    keys = list(ws.wallets.keys())
    w1 = ws[keys[0]]
    w2 = ws[keys[1]]
    print(w1, w2)
    print(w1.address, w2.address)
    # tx = new_transaction(w1.address, w2.address, 17)
    # print(tx.serialize())
